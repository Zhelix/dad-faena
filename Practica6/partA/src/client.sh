#!/bin/bash

#Lamada del Cliente al servidor RMI que permite especificar los parametros

a=`pwd` #Directorio del JAR

server=$1

if [ $# -ge 1 ]; then
		java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=./politiques/client.policy client/EchoRMI localhost
else 
		java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=./politiques/client.policy client/EchoRMI localhost
fi
