#!/bin/bash

#Arrancar el rmiregistry y el servidor bajo localhost
a=`pwd`

if [ $# -ge 1 ]; then
	java -cp . -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=$1 -Djava.security.policy=./politiques/server.policy server/EchoObjectRMI

else 
	java -cp . -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=localhost -Djava.security.policy=./politiques/server.policy server/EchoObjectRMI
fi
