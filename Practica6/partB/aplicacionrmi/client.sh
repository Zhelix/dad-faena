#!/bin/bash

#Lamada del Cliente al servidor RMI que permite especificar los parametros

a=`pwd` #Directorio del JAR

server=$1

if [ $# -ge 1 ]; then
	java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=client.policy client/ComputePi $server 45

else 
	java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=client.policy client/ComputePi localhost 45
fi
