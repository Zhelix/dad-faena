import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
public class DemoServidor {
  public static void main(String[] args) {
    System.out.println("Servidor de rebots en marxa");
    ServerSocket servSock= null;
    Socket s= null;
    int port= 16000;

    //Input and output Stream.
    OutputStream os= null;
    InputStream is;
    try {

      //Creació del socket
      servSock= new ServerSocket(port);
      s= servSock.accept();
      System.out.println("Connexio establida pel servidor");

      //Inicialitza los streams agafant de la conexió
      is= s.getInputStream();
      os= s.getOutputStream();

    } catch (IOException e) {
      System.err.println("Error de connexio");
      return;
    }

    //L'entrada del buffer
    BufferedReader bis= new BufferedReader(new InputStreamReader(is));

    //L'eixida del buffer
    PrintWriter bos= new PrintWriter(os);


    int i= 0;
    for (;;) {
      String inputLine= "";
      try {

        inputLine= bis.readLine();
        if (inputLine == null) return;
        System.out.println("Servidor: "+inputLine);
        bos.println(inputLine);
        bos.flush();
      } catch (IOException e) {
        System.err.println("Error de comunicacio");
        return;
      }
      i++;
    }
  }
}
