package pract5d;

public interface EchoInt extends java.rmi.Remote {
  public String echo(String input) throws java.rmi.RemoteException;
}
