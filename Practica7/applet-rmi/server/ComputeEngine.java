package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import rmi.Compute;
import rmi.Task;

public class  ComputeEngine implements Compute {
	
	Task<Object> carga;

    public ComputeEngine() {
        super();
    }

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub =
                (Compute) UnicastRemoteObject.exportObject(engine, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("Servicio a la escucha. . .");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }

	@Override
	public void loadTask(Task t) throws RemoteException {
		// TODO Auto-generated method stub
		carga = t;
		
	}

	@Override
	public Object executeTask(Object arg) throws RemoteException {
		// TODO Auto-generated method stub
		//System.out.println("Començant a executar la TASK");
		Object resultat = null;
		
		if( carga == null){
			resultat = "Error";
			return resultat;
		}
		
		resultat = carga.execute(arg.toString());
		//System.out.println("Task Executada , resposta enviada\n-------");
		
		return resultat;		
	}



}
