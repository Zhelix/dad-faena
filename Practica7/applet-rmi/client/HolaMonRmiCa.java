package client;
// HolaMonRmiCa.java
/**
 * Aquest programa és el client applet per a la version RMI del missatge
 * de salutació "Hola món applet"
 */             
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.awt.Graphics;
import java.applet.Applet;

import rmi.Compute;



public class HolaMonRmiCa extends Applet {
  // System.setProperty("java.security.policy", "holamon.policy");
  // System.setSecurityManager(new RMISecurityManager());
  // línies anteriors eliminades per les propietats dels applets
  String urlRemot= "rmi://localhost/"; 
  String missatge= "";
  Object m3ju = null;
  private static final long serialVersionUID = 1L;

  public void init() {
    
    try {
       String name = "Compute";
            Compute comp = (Compute)Naming.lookup(urlRemot+name);
            
            //faena de rmi
            sTask task = new sTask();
            comp.loadTask(task);
            m3ju = comp.executeTask("vale");
            //System.out.println(m3ju);

    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void paint(Graphics g) {
    g.drawString(m3ju.toString(),25,50);
  }
}
