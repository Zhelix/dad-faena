package client;

import rmi.Task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

public class sTask implements Task<Object>, Serializable {

    private static final long serialVersionUID = 227L;
    
    public sTask() {}

	public Object execute(Object x) {
		String output="";
		try {
			 BufferedReader br = new BufferedReader(new FileReader("capitulos.m3u"));
			 String line = null;
			 while ((line = br.readLine()) != null) {
			   output=output+line+"\n";
			 }
		} catch(IOException e) {
	        System.out.println("Error durante la lectura del Fichero m3u");
		}
		return output;
	}
	
}
    