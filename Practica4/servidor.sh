#!/bin/bash

#Arrancar el rmiregistry y el servidor bajo localhost
a=`pwd`

if [ $# -ge 1 ]; then
	java -cp compute.jar -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=$1 -Djava.security.policy=server.policy engine.ComputeEngine

else 
	java -cp compute.jar -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=localhost -Djava.security.policy=server.policy engine.ComputeEngine
fi
